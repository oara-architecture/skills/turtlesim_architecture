#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import ReentrantCallbackGroup

from std_srvs.srv import Empty
from turtlesim_managers.srv import Transition

class simulator(Node):

    def __init__(self):
        Node.__init__(self, 'turtlebot_simulator')
        self.get_logger().info("Wait for resource manager")
        cbgroup = ReentrantCallbackGroup()
        self.resource_srv = self.create_client(Transition, '/turtle1/resource/change_simulator', callback_group=cbgroup)
        self.resource_srv.wait_for_service(timeout_sec=None)
        self.sim_srv = self.create_client(
            Empty, '/clear', callback_group=cbgroup)
        self.rate = self.create_timer(2, self.run, callback_group=cbgroup)
        self.get_logger().info("Ready")
        self.available = None

    def run(self):
        if rclpy.ok():
            sim = self.sim_srv.wait_for_service(timeout_sec=1)
            if (self.available is None or not self.available) and sim:
                self.get_logger().info("Simulator available")
                self.resource_srv.call(Transition.Request(target='AVAILABLE'))
                self.available = True
            elif (self.available is None or self.available) and not sim:
                self.get_logger().info("Simulator unavailable")
                self.resource_srv.call(Transition.Request(target='UNAVAILABLE'))
                self.available = False
            
if __name__ == '__main__':
    rclpy.init()
    x = simulator()
    exec = MultiThreadedExecutor(2)
    exec.add_node(x)
    exec.spin()
