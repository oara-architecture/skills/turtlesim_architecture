#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import ReentrantCallbackGroup
from geometry_msgs.msg import Twist
from turtlesim_managers.srv import Transition

class manual(Node):

    def __init__(self):
        Node.__init__(self, 'manual_control_monitor')
        self.get_logger().info("Wait for resource manager")
        cbgroup = ReentrantCallbackGroup()
        self.cmd_subscriber = self.create_subscription(
            Twist, '/turtle1/manual_vel', self.cmd_callback, 1)
        self.resource_srv = self.create_client(
            Transition, '/turtle1/resource/change_motion', callback_group=cbgroup)
        self.resource_srv.wait_for_service(timeout_sec=None)
        self.rate = self.create_timer(2, self.run, callback_group=cbgroup)
        self.teleop = False
        self.manual = None
        self.get_logger().info("Ready")

    def cmd_callback(self, msg):
        self.get_logger().info(f"Received manual cmd {msg.linear.x}")
        self.teleop = True

    def run(self):
        if rclpy.ok():
            if (self.manual is None or not self.manual) and self.teleop:
                self.get_logger().info("Manual control")
                self.resource_srv.call(Transition.Request(target='MANUAL'))
                self.manual = True
            elif (self.manual is None or self.manual) and not self.teleop:
                self.get_logger().info("Manual control released")
                self.resource_srv.call(
                    Transition.Request(target='NOT_MOVING'))
                self.manual = False
            self.teleop = False


if __name__ == '__main__':
    rclpy.init()
    x = manual()
    exec = MultiThreadedExecutor(2)
    exec.add_node(x)
    exec.spin()
