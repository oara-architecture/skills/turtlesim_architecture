#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from turtlesim_managers.srv import Transition

class manual():

    def __init__(self):
        rospy.init_node('manual_control_monitor', anonymous=True)
        rospy.loginfo("Wait for resource manager")
        rospy.wait_for_service('/turtle1/resource/change_motion')
        self.resource_srv = rospy.ServiceProxy('/turtle1/resource/change_motion', Transition)
        self.rate = rospy.Rate(.5)
        self.subscriber = rospy.Subscriber('/turtle1/cmd_vel', Twist, self.cmd_cb)
        self.teleop = False
        rospy.loginfo("Ready")

    def cmd_cb(self, msg):
        if 'teleop' in msg._connection_header['callerid']:
            self.teleop = True

    def run(self):
        manual = False
        while not rospy.is_shutdown():
            if self.teleop:
                if not manual:
                    rospy.loginfo("Manual control taken")
                    self.resource_srv(target='MANUAL')
                    manual = True
            elif manual:
                rospy.loginfo("Manual control released")
                self.resource_srv(target='NOT_MOVING')
                manual = False
            self.teleop = False
            self.rate.sleep()


if __name__ == '__main__':
    try:
        #Testing our function
        x = manual()
        x.run()

    except rospy.ROSInterruptException:
        pass
