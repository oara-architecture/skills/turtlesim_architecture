#!/usr/bin/env python
import rospy
from std_srvs.srv import Empty
from turtlesim_managers.srv import Transition

class simulator():

    def __init__(self):
        rospy.init_node('turtlebot_simulator', anonymous=True)
        rospy.loginfo("Wait for resource manager")
        rospy.wait_for_service('/turtle1/resource/change_simulator')
        self.resource_srv = rospy.ServiceProxy('/turtle1/resource/change_simulator', Transition)
        self.rate = rospy.Rate(.5)
        rospy.loginfo("Ready")

    def run(self):
        available = None
        while not rospy.is_shutdown():
            try:
                rospy.wait_for_service('/clear', timeout=1)
                rospy.loginfo("Simulator available")
                if available is None or not available:
                    self.resource_srv(target='AVAILABLE')
                    available = True
            except rospy.ROSException:
                rospy.loginfo("Simulator unavailable")
                if available is None or available:
                    self.resource_srv(target='UNAVAILABLE')
                    available = False
            self.rate.sleep()


if __name__ == '__main__':
    try:
        #Testing our function
        x = simulator()
        x.run()

    except rospy.ROSInterruptException:
        pass
